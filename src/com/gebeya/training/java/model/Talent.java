package com.gebeya.training.java.model;

/**
 * Created by mativo on 05/12/2016.
 */
public class Talent extends User {
    private int experience;
    private String education;
    private String certification;
    private String languages;

    public static String TABLE = "talent";

    public Talent() {
    }

    public int getExperience() {
        return experience;
    }

    public Talent setExperience(int experience) {
        this.experience = experience;
        return this;
    }

    public String getEducation() {
        return education;
    }

    public Talent setEducation(String education) {
        this.education = education;
        return this;
    }

    public String getCertification() {
        return certification;
    }

    public Talent setCertification(String certification) {
        this.certification = certification;
        return this;
    }

    public String getLanguages() {
        return languages;
    }

    public Talent setLanguages(String languages) {
        this.languages = languages;
        return this;
    }
}
