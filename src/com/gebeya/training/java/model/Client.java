package com.gebeya.training.java.model;

/**
 * Created by mativo on 05/12/2016.
 */
public class Client extends User {
    private String country, city;

    public static String TABLE  = "client";

    public Client() {
    }

    public String getCountry() {
        return country;
    }

    public Client setCountry(String country) {
        this.country = country;
        return this;
    }

    public String getCity() {
        return city;
    }

    public Client setCity(String city) {
        this.city = city;
        return this;
    }
}
