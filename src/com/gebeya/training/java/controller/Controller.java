package com.gebeya.training.java.controller;

import com.gebeya.training.java.db.SQLConnection;

import java.util.ArrayList;

/**
 * Created by mativo on 05/12/2016.
 */
public abstract class Controller<T> {
    protected ArrayList<T> items;
    protected String table;

    protected SQLConnection db;

    public Controller(String table, SQLConnection db) {
        this.table = table;
        this.db = db;

        items = new ArrayList<T>();
    }

    public void getAllModels() {

    }

    public abstract T getModel();

    public abstract boolean save(T t);

    public abstract boolean update(T t);

    public ArrayList<T> getItems(){
        return items;
    }

}
