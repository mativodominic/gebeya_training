package com.gebeya.training.java.controller;

import com.gebeya.training.java.db.SQLConnection;
import com.gebeya.training.java.model.Auth;

/**
 * Created by mativo on 05/12/2016.
 */
public class AuthController extends Controller<Auth> {

    public AuthController(String table, SQLConnection db) {
        super(table, db);
    }

    @Override
    public Auth getModel() {
        return null;
    }

    @Override
    public boolean save(Auth auth) {
        return false;
    }

    @Override
    public boolean update(Auth auth) {

        return false;
    }
}
